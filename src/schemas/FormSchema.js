import { compileToFunctions } from "vue-template-compiler"

export const schema = {
    roof_section_head: {
        step: 1,
        type: "label_field",
        label: 'Section 1 - Roof',
        fieldType: "section-head",
        wrapperStyle: {
            class: 'col s12'
        },
        fieldStyle: {

        },
        validations: {

        }
    },
    // roof_close_desc: {
    //     step: 1,
    //     type: "label_field",
    //     label: 'Closely spaced rafters (no more than 2 feet apart) properly fixed to the ring beam (in the case of masonry/concrete houses) or the wall plate (mainly in the case of timber/wooden houses).',
    //     fieldType: "description",
    //     wrapperStyle: {
    //         class: 'col s12'
    //     },
    //     fieldStyle: {

    //     },
    //     validations: {

    //     }
    // },

    roof_type: {
        step: 1,
        type: "radio_field",
        // fieldType: 'text',
        label: "Roof is one of the following types:",
        placeholder: '',
        helperText: '',
        value: '',
        rFeature: 'roof',
        answer: ['rip_roof', 'concrete_slab_roof', 'strongly_constructed_gable_roof'],
        options: [{
                text: 'Hip Roof',
                value: 'rip_roof'
            },
            {
                text: 'Concrete slab roof',
                value: 'concrete_slab_roof'
            },
            {
                text: 'Strongly constructed gable roof',
                value: 'strongly_constructed_gable_roof'
            },

        ],
        // invalid: true,
        wrapperStyle: {
            class: 'col s12'
        },
        gridStyle: {
            grid: 'col s4'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },
    spaced_rafters: {
        step: 1,
        type: "radio_field",
        // fieldType: 'text',
        label: "Closely spaced rafters",
        placeholder: '',
        helperText: '',

        value: '',
        rFeature: 'roof',
        answer: ['yes'],
        options: [{
                text: 'Yes',
                value: 'yes'
            },
            {
                text: 'No',
                value: 'no'
            },


        ],
        // invalid: true,
        wrapperStyle: {
            class: 'col s12'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },
    roofs_separated: {
        step: 1,
        type: "radio_field",
        // fieldType: 'text',
        label: "porch/veranda separated from main roof",
        placeholder: '',
        helperText: '',

        value: '',
        rFeature: 'roof',
        answer: ['yes'],
        options: [{
                text: 'Yes',
                value: 'yes'
            },
            {
                text: 'No',
                value: 'no'
            },


        ],
        // invalid: true,
        wrapperStyle: {
            class: 'col s12'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },
    // group_label: {
    //     step: 1,
    //     fieldType: 'label_field',
    //     type: 'label',
    //     text: 'Group Test',
    //     group: {
    //         tag: 'group_test',
    //         isHead: true,
    //     },
    //     validations: {}

    // },
    roof_overhangs: {
        step: 1,
        type: "radio_field",
        // fieldType: 'text',
        label: "Roof overhangs",
        placeholder: '',
        helperText: '',
        value: '',
        rFeature: 'roof',
        answer: ['yes'],
        options: [{
                text: 'Yes',
                value: 'yes'
            },
            {
                text: 'No',
                value: 'no'
            },


        ],
        // invalid: true,
        wrapperStyle: {
            class: 'col s12'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },
    roof_covering: {
        step: 1,
        type: "radio_field",
        // fieldType: 'text',
        label: "Roof covering",
        placeholder: '',
        helperText: '',
        value: '',
        rFeature: 'roof',
        answer: ['yes'],
        options: [{
                text: 'Yes',
                value: 'yes'
            },
            {
                text: 'No',
                value: 'no'
            },


        ],
        // invalid: true,
        wrapperStyle: {
            class: 'col s12'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },
    roof_screws: {
        step: 1,
        type: "radio_field",
        // fieldType: 'text',
        label: "Roof screws",
        placeholder: '',
        helperText: '',
        value: '',
        rFeature: 'roof',
        answer: ['yes'],
        options: [{
                text: 'Yes',
                value: 'yes'
            },
            {
                text: 'No',
                value: 'no'
            },


        ],
        // invalid: true,
        wrapperStyle: {
            class: 'col s12'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },
    roof_covering_thin: {
        step: 1,
        type: "radio_field",
        // fieldType: 'text',
        label: "Roof covering thin",
        placeholder: '',
        helperText: '',
        value: '',
        rFeature: 'roof',
        answer: ['yes'],
        options: [{
                text: 'Yes',
                value: 'yes'
            },
            {
                text: 'No',
                value: 'no'
            },


        ],
        // invalid: true,
        wrapperStyle: {
            class: 'col s12'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },
    // group_label: {
    //     step: 1,
    //     fieldType: 'label_field',
    //     type: 'label',
    //     text: 'Group Test',
    //     group: {
    //         tag: 'group_test',
    //         isHead: true,
    //     },
    //     validations: {}

    // },
    // name: {
    //     step: 1,
    //     type: "input_field",
    //     fieldType: 'text',
    //     label: "Name",
    //     placeholder: '',
    //     helperText: '',
    //     value: '',
    //     groupTag: 'group_test',
    //     // condition: {
    //     //     key: 'title',
    //     //     value: 'mr'
    //     // },
    //     // invalid: true,
    //     wrapperStyle: {
    //         class: 'col s12'
    //     },
    //     fieldStyle: {},
    //     validations: {
    //         required: {
    //             params: null,
    //             message: 'Required'
    //         }
    //     },
    // },
    // car_color: {
    //     step: 1,
    //     type: "select_field",
    //     fieldType: 'text',
    //     label: "Car Color",
    //     isMultiple: false,
    //     placeholder: '',
    //     helperText: '',
    //     value: 'red',
    //     groupTag: 'group_test',
    //     // invalid: true,
    //     // condition: {
    //     //     key: 'title',
    //     //     value: 'mr'
    //     // },
    //     options: [{
    //             text: 'Select Color',
    //             default: true

    //         },
    //         {
    //             text: 'Red',
    //             value: 'red'
    //         }, {
    //             text: 'Blue',
    //             value: 'blue'
    //         }, {
    //             text: 'Purple',
    //             value: 'purple'
    //         },
    //         {
    //             text: 'Yellow',
    //             value: 'yellow'
    //         }

    //     ],
    //     wrapperStyle: {
    //         class: 'col s12'
    //     },
    //     fieldStyle: {},
    //     validations: {
    //         // required: {
    //         //     params: null,
    //         //     message: 'Required'
    //         // }
    //     }
    // },
    roof_condition: {
        step: 1,
        type: "radio_field",
        fieldType: 'text',
        label: "How would you describe the general condition of your roof? ",
        placeholder: '',
        helperText: '',
        value: '',

        // invalid: true,
        // condition: {
        //     key: 'title',
        //     value: 'mr'
        // },
        answer: 'isCondition',
        options: [

            {
                text: 'Excellent',
                value: 'excellent'
            }, {
                text: 'Good',
                value: 'good'
            }, {
                text: 'Fair',
                value: 'fair'
            },
            {
                text: 'Poor',
                value: 'poor'
            }
        ],
        wrapperStyle: {
            class: 'col s12'
        },
        gridStyle: {
            grid: 'col s3'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },

    windows_doors_section_head: {
        step: 2,
        type: "label_field",
        label: 'Section 2 - Windows and Doors',
        fieldType: "section-head",
        wrapperStyle: {
            class: 'col s12'
        },
        fieldStyle: {

        },
        validations: {

        }
    },

    group_label_window: {
        step: 2,
        type: 'group_field',
        fieldType: 'label',
        text: 'Which of the following is true about your windows? ',
        group: {
            tag: 'window',
            isHead: true,
        },
        validations: {}

    },


    window_resiliency_features: {
        step: 2,
        type: "checkbox_field",
        fieldType: 'text',
        label: "At least one of the following is present (select all that apply): ",
        placeholder: '',
        helperText: '',
        value: [],
        groupTag: 'window',
        rFeature: 'window',
        options: [

            {
                text: 'If glass, they are impact resistant windows which have a Miami Dade Notice of Acceptance or equivalent standard. (Windows should have come with a removable label when purchased). ',
                value: 'impact_resistant'
            }, {
                text: 'Aluminum louvers',
                value: 'aluminum_louvers'
            }, {
                text: 'Solid wooden shutters',
                value: 'solid_wooden_shutters'
            },
            {
                text: 'Storm/hurricane shutters which have a Miami Dade Notice of Acceptance or equivalent standard',
                value: 'notice_of_acceptance'
            }

        ],
        wrapperStyle: {
            class: 'col s12'
        },
        gridStyle: {
            grid: 'col s12'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },

    windows_installed_place_seal: {
        step: 2,
        type: "radio_field",
        // fieldType: 'text',
        label: "Windows properly installed/fixed in place and sealed.",
        placeholder: '',
        helperText: '',
        value: '',
        groupTag: 'window',
        // condition: {
        //     key: 'roof_overhangs',
        //     value: 'no'
        // },
        rFeature: 'window',
        answer: ['yes'],
        options: [{
                text: 'Yes',
                value: 'yes'
            },
            {
                text: 'No',
                value: 'no'
            },


        ],
        // invalid: true,
        wrapperStyle: {
            class: 'col s12'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }

    },
    group_label_door: {
        step: 2,
        type: 'group_field',
        fieldType: 'label',
        text: 'Which of the following is true about your doors? ',
        group: {
            tag: 'door',
            isHead: true,
        },
        validations: {}

    },
    door_resiliency_features: {
        step: 2,
        type: "checkbox_field",
        fieldType: 'text',
        label: "At least one of the following is present (select all that apply): ",
        placeholder: '',
        helperText: '',
        value: [],
        groupTag: 'door',
        rFeature: 'door',
        options: [

            {
                text: 'If glass, they are impact resistant windows which have a Miami Dade Notice of Acceptance or equivalent standard. (Windows should have come with a removable label when purchased). ',
                value: 'impact_resistant'
            }, {
                text: 'Solid wooden, metal or PVC exterior doors',
                value: 'solid_wooden'
            }, {
                text: 'Solid wooden shutters',
                value: 'solid_wooden_shutters'
            },
            {
                text: 'Storm/hurricane shutters for exterior doors which have a Miami Dade Notice of Acceptance or equivalent standard.',
                value: 'notice_of_acceptance'
            }

        ],
        wrapperStyle: {
            class: 'col s12'
        },
        gridStyle: {
            grid: 'col s12'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },

    window_door_condition: {
        step: 2,
        type: "radio_field",
        fieldType: 'text',
        label: "How would you describe the general condition of your windows and doors? ",
        placeholder: '',
        helperText: '',
        value: '',

        // invalid: true,
        // condition: {
        //     key: 'title',
        //     value: 'mr'
        // },
        answer: 'isCondition',
        options: [

            {
                text: 'Excellent',
                value: 'excellent'
            }, {
                text: 'Good',
                value: 'good'
            }, {
                text: 'Fair',
                value: 'fair'
            },
            {
                text: 'Poor',
                value: 'poor'
            }

        ],
        wrapperStyle: {
            class: 'col s12'
        },
        gridStyle: {
            grid: 'col s3'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },
    walls_foundations_section_head: {
        step: 3,
        type: "label_field",
        label: 'Section 3 - Walls and Foundations',
        fieldType: "section-head",
        wrapperStyle: {
            class: 'col s12'
        },
        fieldStyle: {

        },
        validations: {

        }
    },
    wall_condition: {
        step: 3,
        type: "radio_field",
        fieldType: 'text',
        label: "Based on your review of these examples and an inspection of your house, how would you describe the general strength and condition of your walls? ",
        placeholder: '',
        helperText: '',
        value: '',
        answer: 'isCondition',
        // invalid: true,
        // condition: {
        //     key: 'title',
        //     value: 'mr'
        // },
        options: [

            {
                text: 'Excellent',
                value: 'excellent'
            }, {
                text: 'Good',
                value: 'good'
            }, {
                text: 'Fair',
                value: 'fair'
            },
            {
                text: 'Poor',
                value: 'poor'
            }

        ],
        wrapperStyle: {
            class: 'col s12'
        },
        gridStyle: {
            grid: 'col s3'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },
    foundation_condition: {
        step: 3,
        type: "radio_field",
        fieldType: 'text',
        label: "Based on your review of these examples and an inspection of your house, how would you describe the general strength and condition of your foundation?",
        placeholder: '',
        helperText: '',
        value: '',
        answer: 'isCondition',
        // invalid: true,
        // condition: {
        //     key: 'title',
        //     value: 'mr'
        // },
        options: [

            {
                text: 'Excellent',
                value: 'excellent'
            }, {
                text: 'Good',
                value: 'good'
            }, {
                text: 'Fair',
                value: 'fair'
            },
            {
                text: 'Poor',
                value: 'poor'
            }

        ],
        wrapperStyle: {
            class: 'col s12'
        },
        gridStyle: {
            grid: 'col s3'
        },
        fieldStyle: {},
        validations: {
            required: {
                params: null,
                message: 'Required'
            }
        }
    },

}


export const impactLevelGenerator = (schema, values) => {
    // let impactLevelArray = []
    let rFeaturesImpactLevelBoundary = {
        minor: 10,
        moderate: 6,
        major: 1,
        extreme: 0,
    }

    let totalPoints = 0


    const getTotalPoints = () => {
        Object.keys(values).map(key => {
            if (schema[key].type === 'checkbox_field') {
                if (values[key].length > 0) {
                    totalPoints++
                }
            } else if (schema[key].answer === 'isCondition') {

                if (key !== 'window_door_condition') {
                    if (['excellent', 'good'].includes(values[key])) {
                        totalPoints += 2
                    } else if (['fair'].includes(values[key])) {
                        totalPoints++
                    }
                }

            } else {
                if (schema[key].answer.includes(values[key])) {
                    totalPoints++;
                }
            }
        })
    }

    getTotalPoints()

    // console.log(totalPoints)


    // ROOF FEATURES

    // Get all roof feature question keys
    let roofFeatureQuestions = Object.keys(schema).filter(key => schema[key].rFeature === 'roof')

    // Get all window door feature questions
    let windowDoorFeaturesQuestions = Object.keys(schema).filter(key => schema[key].rFeature === 'window' || schema[key].rFeature === 'door')


    // Get roof features answered questions
    let ansRoofFeaturesQuestions = roofFeatureQuestions.filter(key =>
        !!values[key] && values[key] !== 'no')




    const checkBoxValidator = (value) => {

        return value && value.length > 0 ? true : false
    }

    // Get window door features answered questions
    let ansWindowDoorFeaturesQuestions = windowDoorFeaturesQuestions.filter(key => {

        if (schema[key].type === 'checkbox_field') {
            // console.log(values[key])

            if (checkBoxValidator(values[key])) {
                return key
            }

        } else {

            if (values[key] === 'yes') {
                return key
            }
        }
    })



    const minorConditionChecker = () => {
        return ((values.roof_condition === 'excellent' || values.roof_condition === 'good') && (values.wall_condition === 'excellent' || values.wall_condition === 'good') && (values.foundation_condition === 'excellent' || values.foundation_condition === 'good')) ? true : false;
    }

    const moderateConditionChecker = () => {
        return ((values.wall_condition === 'excellent' || values.wall_condition === 'good') && (values.foundation_condition === 'excellent' || values.foundation_condition === 'good')) ? true : false;
    }
    const majorConditionChecker = () => {
        return ((values.wall_condition && values.wall_condition !== 'poor') && (values.foundation_condition && values.foundation_condition !== 'poor')) ? true : false;
    }
    const extremeConditionChecker = () => {
        return ((values.wall_condition && values.wall_condition === 'poor') && (values.foundation_condition && values.foundation_condition === 'poor')) ? true : false;
    }


    const roofFeaturesModerateChecker = () => {


        if (!ansRoofFeaturesQuestions.includes(roofFeatureQuestions[4]) ||
            !ansRoofFeaturesQuestions.includes(roofFeatureQuestions[5]) || !ansRoofFeaturesQuestions.includes(roofFeatureQuestions[6])) return true

        return false
    }


    const roofFeaturesMajorChecker = () => {

        if (!ansRoofFeaturesQuestions.includes(roofFeatureQuestions[0]) || !ansRoofFeaturesQuestions.includes(roofFeatureQuestions[1]) || !ansRoofFeaturesQuestions.includes(roofFeatureQuestions[2]) || !ansRoofFeaturesQuestions.includes(roofFeatureQuestions[3])) return true;
        return false
    }

    const minorRFeaturesChecker = () => {
        let featuresCount = ansRoofFeaturesQuestions.length + ansWindowDoorFeaturesQuestions.length
        return featuresCount === rFeaturesImpactLevelBoundary.minor ? true : false
    }

    const moderateRFeaturesChecker = () => {
        let featuresCount = ansRoofFeaturesQuestions.length


        return (featuresCount === rFeaturesImpactLevelBoundary.moderate && roofFeaturesModerateChecker()) ? true : false;

    }
    const majorRFeaturesChecker = () => {

        return (roofFeaturesMajorChecker()) ? true : false
    }
    const extremeRFeaturesChecker = () => {
        let featuresCount = ansRoofFeaturesQuestions.length + ansWindowDoorFeaturesQuestions.length

        return (featuresCount <= 0) ? true : false
    }



    // Impact Level Check
    if (minorRFeaturesChecker() && minorConditionChecker()) { // MINOR
        // console.log(`minor => ${minorRFeaturesChecker() && minorConditionChecker()}`)
        return 'minor'
    } else if (moderateRFeaturesChecker() && moderateConditionChecker()) { // MODERATE
        // console.log(`moderate => ${moderateRFeaturesChecker() && moderateConditionChecker()}`)
        return 'moderate'
    } else if (majorRFeaturesChecker() && majorConditionChecker()) { // MAJOR
        // console.log(`major => ${majorRFeaturesChecker() && majorConditionChecker()}`)
        return 'major'

    } else if (extremeRFeaturesChecker() && extremeConditionChecker()) { // EXTREME
        // console.log(`extreme => ${extremeRFeaturesChecker() && extremeConditionChecker()}`)
        return 'extreme'

    } else {
        if (totalPoints >= 16) {
            return 'minor'
        } else if (totalPoints < 16 && totalPoints >= 13) {
            return 'moderate'
        } else if (totalPoints < 13 && totalPoints >= 1) {
            return 'major'
        } else {
            return 'extreme'
        }

        // return 'determine with point checker'

    }
    // console.log(schema)
    // console.log(roofFeatureQuestions)
}